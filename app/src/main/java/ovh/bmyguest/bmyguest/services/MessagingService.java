package ovh.bmyguest.bmyguest.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.utils.Tools;

public class MessagingService extends FirebaseMessagingService {
    public static Map<String, ArrayList<String>> notifications = new HashMap<>();
    public static int maxLines = 5;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (getSharedPreferences("user_details", Context.MODE_PRIVATE).getInt("id", 0) == Integer.parseInt(remoteMessage.getData().get("userId"))) {
            return;
        }

        ArrayList<String> lines;

        if (!notifications.containsKey(remoteMessage.getData().get("tag"))) {
            notifications.put(remoteMessage.getData().get("tag"), new ArrayList<String>());
        }

        String message = Integer.parseInt(remoteMessage.getData().get("type")) == 666 ? remoteMessage.getData().get("title") : remoteMessage.getData().get("body");
        String text = Integer.parseInt(remoteMessage.getData().get("type")) == 666 ? " new article" : " new comment";
        String summaryText = Integer.parseInt(remoteMessage.getData().get("type")) == 666 ? " more article" : " more comment";

        lines = notifications.get(remoteMessage.getData().get("tag"));
        lines.add(message);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_ic_notif)
                .setContentTitle(remoteMessage.getData().get("category"))
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentText(lines.size() + text + (lines.size() > 1 ? "s" : ""))
                .setLargeIcon(Tools.getBitmapFromURL(remoteMessage.getData().get("pictureUrl")));

        for (int i = 0; i < Math.min(lines.size(), maxLines); i++) {
            inboxStyle.addLine(lines.get(i));
        }

        if (lines.size() > maxLines) {
            inboxStyle.setSummaryText("+" + (lines.size() - maxLines) + summaryText + (lines.size() - maxLines > 1 ? "s" : ""));
        }

        inboxStyle.setBigContentTitle(remoteMessage.getData().get("category"));
        builder.setStyle(inboxStyle);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setAction(Long.toString(System.currentTimeMillis()));

//        if (Integer.parseInt(remoteMessage.getData().get("type")) == 667) {
//            Event article = new Event();
//            article.setId(Integer.parseInt(remoteMessage.getData().get("articleId")));
//            resultIntent.putExtra("navigate", "CommentFragment");
//            resultIntent.putExtra("article", article);
//        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        nManager.notify(Integer.parseInt(remoteMessage.getData().get("tag")), builder.build());
    }
}
