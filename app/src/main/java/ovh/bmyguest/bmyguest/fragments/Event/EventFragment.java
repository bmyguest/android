package ovh.bmyguest.bmyguest.fragments.Event;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.EventViewPagerAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Event;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;
import ovh.bmyguest.bmyguest.views.CircularNetworkImageView;

public class EventFragment extends Fragment {
    private static final String ARG_EVENT = "event";
    public Event event;
    private EventInteractionsListener listener;
    private ViewPager viewPager;
    private int currentPage = 0;
    private Button btnJoin;
    private Button btnManage;
    private Button btnLeave;
    private User loggedUser;

    public static EventFragment newInstance(Event event) {
        EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_EVENT, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        event = getArguments() != null ? (Event) getArguments().getParcelable(ARG_EVENT) : null;
        loggedUser = ((MainActivity) getActivity()).user;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(0);
            actionBar.setTitle(event.getName());
        }

        displayEvent();
        listener.getEvent(event);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventInteractionsListener) {
            listener = (EventInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement EventInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void displayEvent() {
        View view = getView();
        if (view == null) {
            return;
        }

        CircularNetworkImageView ownerPicture = view.findViewById(R.id.owner_picture);
        ownerPicture.setImageUrl(event.getOwner().getPicture(), ApiRequestManager.getInstance(getActivity()).getImageLoader());
        ownerPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOwnerPictureClicked(event.getOwner());
            }
        });

        btnJoin = view.findViewById(R.id.btn_join);
        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                participateEvent(event);
            }
        });

        btnLeave = view.findViewById(R.id.btn_leave);
        btnLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leaveEvent(event);
            }
        });

        btnManage = view.findViewById(R.id.btn_manage);
        btnManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onManage(event);
            }
        });

        updateButtons(loggedUser.isParticipant(event));

        ((NetworkImageView) view.findViewById(R.id.event_picture)).setImageUrl(event.getPicture().getFile(), ApiRequestManager.getInstance(getActivity()).getImageLoader());
        ((TextView) view.findViewById(R.id.event_name)).setText(event.getName());
        ((TextView) view.findViewById(R.id.event_description)).setText(Tools.decodeBase64String(event.getDescription()));
        ((TextView) view.findViewById(R.id.owner_username)).setText(getString(R.string.fragment_event_owner_name, event.getOwner().getName()));

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.removeAllTabs();
        tabLayout.addTab(tabLayout.newTab().setText("Medias"));
        tabLayout.addTab(tabLayout.newTab().setText("Participants"));
        tabLayout.addTab(tabLayout.newTab().setText("Evaluations"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.viewpager);
        EventViewPagerAdapter adapterViewPager = new EventViewPagerAdapter(getActivity().getSupportFragmentManager(), event, tabLayout.getTabCount());
        viewPager.setAdapter(adapterViewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                if (viewPager.getCurrentItem() != 0) {
                    currentPage = viewPager.getCurrentItem();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setCurrentItem(currentPage);
    }

    public interface EventInteractionsListener {
        void getEvent(Event event);
        void onOwnerPictureClicked(User user);
        void onManage(Event event);
    }

    private void updateButtons(boolean isMember) {
        if (event.getOwner().getId().equals(loggedUser.getId())) {
            btnJoin.setVisibility(View.GONE);
            btnLeave.setVisibility(View.GONE);
            btnManage.setVisibility(View.VISIBLE);
        } else {
            btnJoin.setVisibility(isMember ? View.GONE : View.VISIBLE);
            btnLeave.setVisibility(isMember ? View.VISIBLE : View.GONE);
        }
    }

    public void participateEvent(Event event) {
        HashMap<String, String> data = new HashMap<>();
        data.put("status", "1");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, getString(R.string.general_api_url) + getString(R.string.general_api_url_event_participants, event.getId()), new JSONObject(data), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                updateButtons(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", loggedUser.getToken());
                return headers;
            }
        };

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void leaveEvent(Event event) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, getString(R.string.general_api_url) + getString(R.string.general_api_url_event_participant, event.getId(), loggedUser.getId()), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int count = 0;
                try {
                    count = response.getInt("count");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (count == 0) {
                    return;
                }

                updateButtons(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", loggedUser.getToken());
                return headers;
            }
        };

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}