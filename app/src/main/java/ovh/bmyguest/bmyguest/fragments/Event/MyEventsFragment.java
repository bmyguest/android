package ovh.bmyguest.bmyguest.fragments.Event;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.EventListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Event;
import ovh.bmyguest.bmyguest.parcelables.User;

public class MyEventsFragment extends ListFragment implements SearchView.OnQueryTextListener {
    private static final String ARG_USER = "user";
    public EventListAdapter eventListAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<Event> eventsList = new ArrayList<>();
    private User user;
    private EventsInteractionListener listener;
    private ActionBar actionBar;

    public static MyEventsFragment newInstance(User user) {
        MyEventsFragment fragment = new MyEventsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);

            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                actionBar.setHomeAsUpIndicator(0);
            } else {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }

            actionBar.setTitle(getString(R.string.fragment_my_events_title, eventsList.size()));
        }

        view.findViewById(R.id.button_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCreateEventButtonPressed(user);
            }
        });

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUserEvents(user);
            }
        });

        registerForContextMenu(this.getListView());
        eventListAdapter = new EventListAdapter(getActivity(), eventsList);
        setListAdapter(eventListAdapter);
        getUserEvents(user);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = this.getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.context_menu_fragment_event, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Event event = ((EventListAdapter) getListAdapter()).getItem(info.position);

        switch (item.getItemId()) {
            case R.id.action_manage_event:
                listener.onManageEvent(event);
                return true;
            case R.id.action_invite_contacts:
                listener.onEventInviteContacts(event);
                return true;
            case R.id.action_view_event:
                listener.onUserEventSelected(event);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventsInteractionListener) {
            listener = (EventsInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement EventsInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Event event = eventListAdapter.getItem(position);
        listener.onUserEventSelected(event);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_my_events, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        getUserEventsSuggestions(user, query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        getUserEventsSuggestions(user, newText);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public interface EventsInteractionListener {
        void onManageEvent(Event event);
        void onUserEventSelected(Event event);
        void onCreateEventButtonPressed(User user);
        void onEventInviteContacts(Event event);
    }

    public void getUserEventsSuggestions(User user, String query) {
        GsonRequest<Event[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_events_suggestions, user.getId(), query), Event[].class, new Response.Listener<Event[]>() {
            @Override
            public void onResponse(Event[] events) {
                eventsList.clear();
                eventsList.addAll(Arrays.asList(events));
                eventListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void getUserEvents(User user) {
        GsonRequest<Event[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_events, user.getId()), Event[].class, new Response.Listener<Event[]>() {
            @Override
            public void onResponse(Event[] events) {
                eventsList.clear();
                eventsList.addAll(Arrays.asList(events));
                eventListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                actionBar.setTitle(getString(R.string.fragment_my_events_title, eventsList.size()));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
