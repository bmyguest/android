package ovh.bmyguest.bmyguest.fragments.Event;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.EventListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Event;
import ovh.bmyguest.bmyguest.parcelables.User;

public class ParticipationsFragment extends ListFragment implements SearchView.OnQueryTextListener {
    private static final String ARG_USER = "user";
    public EventListAdapter listAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<Event> list = new ArrayList<>();
    private User user;
    private ParticipationInteractionsListener listener;
    private ActionBar actionBar;

    public static ParticipationsFragment newInstance(User user) {
        ParticipationsFragment fragment = new ParticipationsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        list = user != null ? user.getParticipations() : new ArrayList<Event>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);

            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                actionBar.setHomeAsUpIndicator(0);
            } else {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }

            actionBar.setTitle(getString(R.string.fragment_participations_title, list.size()));
        }

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getParticipations(user);
            }
        });

        registerForContextMenu(this.getListView());
        listAdapter = new EventListAdapter(getActivity(), list);
        setListAdapter(listAdapter);
        getParticipations(user);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = this.getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.context_menu_fragment_event, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Event event = ((EventListAdapter) getListAdapter()).getItem(info.position);

        switch (item.getItemId()) {
            case R.id.action_manage_event:
                listener.onManageEvent(event);
                return true;
            case R.id.action_invite_contacts:
                listener.onEventInviteContacts(event);
                return true;
            case R.id.action_view_event:
                listener.onUserEventSelected(event);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ParticipationInteractionsListener) {
            listener = (ParticipationInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ParticipationInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Event participation = listAdapter.getItem(position);
        listener.onUserEventSelected(participation);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_participations, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        getParticipationsSuggestions(user, query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        getParticipationsSuggestions(user, newText);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public interface ParticipationInteractionsListener {
        void onManageEvent(Event event);
        void onUserEventSelected(Event event);
        void onEventInviteContacts(Event event);
    }

    public void getParticipationsSuggestions(User user, String query) {
        GsonRequest<Event[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_participations_suggestions, user.getId(), query), Event[].class, new Response.Listener<Event[]>() {
            @Override
            public void onResponse(Event[] events) {
                list.clear();
                list.addAll(Arrays.asList(events));
                listAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void getParticipations(User user) {
        GsonRequest<Event[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_participations, user.getId()), Event[].class, new Response.Listener<Event[]>() {
            @Override
            public void onResponse(Event[] events) {
                list.clear();
                list.addAll(Arrays.asList(events));
                listAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                actionBar.setTitle(getString(R.string.fragment_participations_title, list.size()));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
