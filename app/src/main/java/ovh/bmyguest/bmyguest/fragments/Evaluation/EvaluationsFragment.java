package ovh.bmyguest.bmyguest.fragments.Evaluation;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.EvaluationListAdapter;
import ovh.bmyguest.bmyguest.parcelables.Evaluation;
import ovh.bmyguest.bmyguest.parcelables.User;

public class EvaluationsFragment extends ListFragment {
    private static final String ARG_USER = "user";
    public EvaluationListAdapter evaluationsListAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<Evaluation> evaluationsList;
    private User user;
    private EvaluationInteractionsListener listener;

    public static EvaluationsFragment newInstance(User user) {
        EvaluationsFragment fragment = new EvaluationsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        evaluationsList = user != null ? user.getEvaluations() : new ArrayList<Evaluation>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_evaluations, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);

            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                actionBar.setHomeAsUpIndicator(0);
            } else {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }

            actionBar.setTitle(R.string.fragment_evaluations_title);
        }

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_evaluations);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listener.getEvaluations(user);
            }
        });

        evaluationsListAdapter = new EvaluationListAdapter(getActivity(), evaluationsList);
        setListAdapter(evaluationsListAdapter);
        listener.getEvaluations(user);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EvaluationInteractionsListener) {
            listener = (EvaluationInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement EvaluationInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Evaluation evaluation = (Evaluation) evaluationsListAdapter.getItem(position);
        listener.onEvaluationSelected(user, evaluation);
    }

    public interface EvaluationInteractionsListener {
        void onEvaluationSelected(User user, Evaluation evaluation);
        void getEvaluations(User user);
    }
}
