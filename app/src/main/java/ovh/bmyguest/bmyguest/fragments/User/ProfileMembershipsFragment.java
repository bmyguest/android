package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.GroupListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Group;
import ovh.bmyguest.bmyguest.parcelables.User;

public class ProfileMembershipsFragment extends ListFragment {
    private static final String ARG_USER = "user";
    public GroupListAdapter groupsListAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<Group> groupsList = new ArrayList<>();
    private User user;
    private GroupInteractionsListener listener;

    public static ProfileMembershipsFragment newInstance(User user) {
        ProfileMembershipsFragment fragment = new ProfileMembershipsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        groupsList = user != null ? user.getMemberships() : new ArrayList<Group>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_memberships, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMemberships(user);
            }
        });

        groupsListAdapter = new GroupListAdapter(getActivity(), groupsList);
        setListAdapter(groupsListAdapter);
        getMemberships(user);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GroupInteractionsListener) {
            listener = (GroupInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement GroupInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Group group = groupsListAdapter.getItem(position);
        listener.onGroupSelected(group);
    }

    public interface GroupInteractionsListener {
        void onGroupSelected(Group group);
    }

    public void getMemberships(final User user) {
        GsonRequest<Group[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_memberships, user.getId()), Group[].class, new Response.Listener<Group[]>() {
            @Override
            public void onResponse(Group[] groups) {
                user.getMemberships().clear();
                user.getMemberships().addAll(Arrays.asList(groups));
                groupsList.clear();
                groupsList.addAll(Arrays.asList(groups));
                groupsListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
