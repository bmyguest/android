package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.ProfileViewPagerAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;
import ovh.bmyguest.bmyguest.views.CircularNetworkImageView;

public class ProfileFragment extends Fragment {
    private static final String ARG_USER = "user";
    public User user;
    private ProfileInteractionsListener listener;
    private int currentPage = 0;
    private ViewPager profileViewPager;

    public static ProfileFragment newInstance(User user) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);

            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                actionBar.setHomeAsUpIndicator(0);
            } else {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }

            actionBar.setTitle(user.getName());
        }

        displayProfile();
        listener.getProfile(user);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileInteractionsListener) {
            listener = (ProfileInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ProfileInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void displayProfile() {
        if (getView() == null) {
            return;
        }

        ((CircularNetworkImageView) getView().findViewById(R.id.profile_picture)).setImageUrl(user.getPicture(), ApiRequestManager.getInstance(getActivity()).getImageLoader());
        ((TextView) getView().findViewById(R.id.profile_details)).setText(Tools.capitalize(user.getGender()) + ", " + user.getAge() + ", " + user.getLocation());
        ((TextView) getView().findViewById(R.id.profile_note)).setText(String.valueOf(user.getNote()));

        TabLayout tabLayout = getView().findViewById(R.id.tab_layout);
        tabLayout.removeAllTabs();
        tabLayout.addTab(tabLayout.newTab().setText("Contacts"));
        tabLayout.addTab(tabLayout.newTab().setText("Medias"));
        tabLayout.addTab(tabLayout.newTab().setText("Groups"));
        tabLayout.addTab(tabLayout.newTab().setText("Evaluations"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        profileViewPager = getView().findViewById(R.id.profile_viewpager);
        ProfileViewPagerAdapter adapterViewPager = new ProfileViewPagerAdapter(getActivity().getSupportFragmentManager(), user, tabLayout.getTabCount());
        profileViewPager.setAdapter(adapterViewPager);
        profileViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                profileViewPager.setCurrentItem(tab.getPosition());

                if (profileViewPager.getCurrentItem() != 0) {
                    currentPage = profileViewPager.getCurrentItem();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        profileViewPager.setCurrentItem(currentPage);
    }

    public interface ProfileInteractionsListener {
        void getProfile(User user);
    }
}
