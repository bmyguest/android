package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.EvaluationListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Evaluation;
import ovh.bmyguest.bmyguest.parcelables.Event;

public class EventEvaluationsFragment extends ListFragment {
    private static final String ARG_EVENT = "event";
    public EvaluationListAdapter listAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    private Event event;
    public ArrayList<Evaluation> evaluations;
    private EvaluationInteractionsListener listener;

    public static EventEvaluationsFragment newInstance(Event event) {
        EventEvaluationsFragment fragment = new EventEvaluationsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_EVENT, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        event = getArguments() != null ? (Event) getArguments().getParcelable(ARG_EVENT) : null;
        evaluations = event != null ? event.getEvaluations() : new ArrayList<Evaluation>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_evaluations, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_evaluations);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getEvaluations(event);
            }
        });

        listAdapter = new EvaluationListAdapter(getActivity(), evaluations);
        setListAdapter(listAdapter);
        getEvaluations(event);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EvaluationInteractionsListener) {
            listener = (EvaluationInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement EvaluationInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Evaluation evaluation = (Evaluation) listAdapter.getItem(position);
        listener.onEvaluationSelected(event, evaluation);
    }

    public interface EvaluationInteractionsListener {
        void onEvaluationSelected(Event event, Evaluation evaluation);
    }

    public void getEvaluations(final Event event) {
        GsonRequest<Evaluation[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_event_evaluations, event.getId()), Evaluation[].class, new Response.Listener<Evaluation[]>() {
            @Override
            public void onResponse(Evaluation[] evaluations) {
                event.getEvaluations().clear();
                event.getEvaluations().addAll(Arrays.asList(evaluations));
                EventEvaluationsFragment.this.evaluations.clear();
                EventEvaluationsFragment.this.evaluations.addAll(Arrays.asList(evaluations));
                listAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
