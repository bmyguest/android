package ovh.bmyguest.bmyguest.fragments.Media;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.MediaViewPagerAdapter;
import ovh.bmyguest.bmyguest.parcelables.Media;

public class GalleryFragment extends Fragment {
    private static final String ARG_MEDIAS_LIST = "MEDIAS_LIST";
    private static final String ARG_CURRENT_POSITION = "CURRENT_POSITION";
    private ArrayList<Media> mediasList;
    private int currentPosition = 0;
    private TextView galleryIndicator;

    public static GalleryFragment newInstance(ArrayList<Media> mediasList, int position) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_MEDIAS_LIST, mediasList);
        args.putInt(ARG_CURRENT_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mediasList = getArguments() != null ? getArguments().<Media>getParcelableArrayList(ARG_MEDIAS_LIST) : new ArrayList<Media>();
        currentPosition = getArguments() != null ? getArguments().getInt(ARG_CURRENT_POSITION, 0) : 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setHomeAsUpIndicator(0);
//            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#C0000000")));
//            actionBar.setTitle(R.string.fragment_medias_title);
        }

        displayMedia();
    }

    public void displayMedia() {
        View view = getView();
        if (view == null) {
            return;
        }

        galleryIndicator = view.findViewById(R.id.gallery_indicator);
        galleryIndicator.setText(getString(R.string.fragment_gallery_indicator, currentPosition + 1, mediasList.size()));

        final ViewPager viewPager = view.findViewById(R.id.gallery_viewpager);
        MediaViewPagerAdapter adapterViewPager = new MediaViewPagerAdapter(getActivity(), mediasList);
        viewPager.setAdapter(adapterViewPager);
        viewPager.setCurrentItem(currentPosition);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                galleryIndicator.setText(getString(R.string.fragment_gallery_indicator, position + 1, mediasList.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}
