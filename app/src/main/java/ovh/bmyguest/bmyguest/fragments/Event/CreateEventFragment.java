package ovh.bmyguest.bmyguest.fragments.Event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.PlaceAutocompletionAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.network.MultiPartRequest;
import ovh.bmyguest.bmyguest.parcelables.CustomPlace;
import ovh.bmyguest.bmyguest.parcelables.Event;
import ovh.bmyguest.bmyguest.parcelables.Media;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;

import static android.app.Activity.RESULT_OK;

public class CreateEventFragment extends Fragment {
    private static final String ARG_USER = "user";
    private static final int PICK_IMAGE_REQUEST = 6661;
    private User user;
    private Event event;
    private ImageView eventPicture;
    private EditText eventName;
    private EditText eventDescription;
    private EditText eventCapacity;
    private Spinner eventVisibility;
    private EditText eventStart;
    private EditText eventStartHour;
    private EditText eventEnd;
    private EditText eventEndHour;
    private AutoCompleteTextView eventAddress;
    private ArrayList<CustomPlace> places = new ArrayList<>();
    private PlaceAutocompletionAdapter placeAutocompletionAdapter;
    private boolean isPosting = false;

    DatePickerDialog.OnDateSetListener pickEventStartDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Date date = new GregorianCalendar(year, month, dayOfMonth).getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM. yyyy", Locale.getDefault());
            String format = formatter.format(date);
            eventStart.setText(format);
        }
    };

    DatePickerDialog.OnDateSetListener pickEventEndDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Date date = new GregorianCalendar(year, month, dayOfMonth).getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM. yyyy", Locale.getDefault());
            String format = formatter.format(date);
            eventEnd.setText(format);
        }
    };

    TimePickerDialog.OnTimeSetListener pickEventEndHour = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            eventEndHour.setText(String.format(Locale.getDefault(), "%02d", hourOfDay) + ":" + String.format(Locale.getDefault(), "%02d", minutes));
        }
    };

    TimePickerDialog.OnTimeSetListener pickEventStartHour = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            eventStartHour.setText(String.format(Locale.getDefault(), "%02d", hourOfDay) + ":" + String.format(Locale.getDefault(), "%02d", minutes));
        }
    };

    public static CreateEventFragment newInstance(User user) {
        CreateEventFragment fragment = new CreateEventFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        event = new Event();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_event_create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_validate:
                if (!isPosting && checkInputValues()) {
                    isPosting = true;
                    setEventValues();
                    uploadPictureAndCreateEvent();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_create, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(0);
            actionBar.setTitle(R.string.fragment_create_event_title);
        }

        displayForm();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                eventPicture.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void displayForm() {
        View view = getView();
        if (view == null) {
            return;
        }

        final Calendar calendar = Calendar.getInstance(Locale.getDefault());
        eventStart = view.findViewById(R.id.new_event_start);
        eventEnd = view.findViewById(R.id.new_event_end);
        eventEndHour = view.findViewById(R.id.new_event_endHour);
        eventStartHour = view.findViewById(R.id.new_event_startHour);
        eventName = view.findViewById(R.id.new_event_name);
        eventDescription = view.findViewById(R.id.new_event_description);
        eventCapacity = view.findViewById(R.id.new_event_capacity);
        eventVisibility = view.findViewById(R.id.new_event_visibility);
        eventAddress = view.findViewById(R.id.place_autocomplete_text_view);
        placeAutocompletionAdapter = new PlaceAutocompletionAdapter(getContext(), R.layout.row_place_autocompletion, places);
        eventAddress.setAdapter(placeAutocompletionAdapter);
        eventPicture = view.findViewById(R.id.event_picture);
        eventPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        eventAddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                eventAddress.showDropDown();
                return false;
            }
        });

        eventAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onLocationChosen(((CustomPlace) adapterView.getItemAtPosition(i)));
            }
        });

        eventAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                inputToAutoComplete(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        eventStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), pickEventStartDate, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        eventEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), pickEventEndDate, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        eventEndHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), pickEventEndHour, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        eventStartHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), pickEventStartHour, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });
    }

    private boolean checkInputValues() {
        if (eventName.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid name", Toast.LENGTH_LONG).show();
            return false;
        }

        if (eventAddress.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid address", Toast.LENGTH_LONG).show();
            return false;
        }

        if (eventDescription.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid description", Toast.LENGTH_LONG).show();
            return false;
        }

        if (eventStart.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid start day", Toast.LENGTH_LONG).show();
            return false;
        }

        if (eventStartHour.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid start hour", Toast.LENGTH_LONG).show();
            return false;
        }

        if (eventEnd.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid end day", Toast.LENGTH_LONG).show();
            return false;
        }

        if (eventEndHour.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid end hour", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private void setEventValues() {
        event.setName(eventName.getText().toString());
        event.setDescription(Tools.encodeBase64String(eventDescription.getText().toString()));
        event.setVisibility(eventVisibility.getSelectedItemPosition());
        event.setOwner(user);

        if (!eventCapacity.getText().toString().isEmpty()) {
            event.setCapacity(Integer.parseInt(eventCapacity.getText().toString()));
        }

        try {
            String eventStartString = eventStart.getText().toString() + " " + eventStartHour.getText().toString();
            String eventEndString = eventEnd.getText().toString() + " " + eventEndHour.getText().toString();
            event.setStart(new SimpleDateFormat("dd MMM. yyyy HH:mm", Locale.getDefault()).parse(eventStartString));
            event.setEnd(new SimpleDateFormat("dd MMM. yyyy HH:mm", Locale.getDefault()).parse(eventEndString));
        } catch (ParseException e) {
            Toast.makeText(getContext(), "Error on Event Dates format", Toast.LENGTH_SHORT).show();
        } finally {
            isPosting = false;
        }
    }

    private void uploadPictureAndCreateEvent() {
        MultiPartRequest multipartRequest = new MultiPartRequest(Request.Method.POST, getString(R.string.general_api_url) + getString(R.string.general_api_url_medias_upload), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    Media media = new Gson().fromJson(json, Media.class);
                    event.setPicture(media);
                    createEvent(event);
                } catch (UnsupportedEncodingException | JsonSyntaxException error) {
                    Toast.makeText(getContext(), "Something went wrong with your picture :(", Toast.LENGTH_LONG).show();
                } finally {
                    isPosting = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
                isPosting = false;
            }
        }) {
            @Override
            public Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("picture", new DataPart("picture.jpg", Tools.getFileDataFromDrawable(eventPicture.getDrawable()), "image/jpeg"));
                return params;
            }
        };

        ApiRequestManager.getInstance(getContext()).getRequestQueue().add(multipartRequest);
    }

    public void createEvent(Event event) {
        HashMap<String, String> data = event.toHashMap();

        GsonRequest<Event> request = new GsonRequest<>(Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_events), Event.class, new Response.Listener<Event>() {
            @Override
            public void onResponse(Event response) {
                getActivity().onBackPressed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
                isPosting = false;
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void inputToAutoComplete(String tempInput) {
        String key = "key=" + getResources().getString(R.string.api_key_place);
        String input = "input=" + tempInput;
        String types="types=geocode";
        String requestStringRoot = getString(R.string.place_api_autocomplete_url);
        String requestStringFinal = requestStringRoot + "?"+ input + "&" + key + "&" + types;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, requestStringFinal, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray predictions = response.getJSONArray("predictions");

                    if (predictions.length() <= 0) {
                        return;
                    }

                    places.clear();

                    for (int i = 0; i < predictions.length(); i++) {
                        JSONObject prediction = predictions.getJSONObject(i);
                        CustomPlace customPlace = new CustomPlace(prediction.getString("place_id"), prediction.getString("description"));
                        places.add(customPlace);
                    }

                    placeAutocompletionAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void onLocationChosen(final CustomPlace chosenPlace) {
        String url = getString(R.string.place_api_details_url, chosenPlace.getId(), getString(R.string.api_key_place));

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject rawLocation = response.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
                    event.setAddress(chosenPlace.getDescription());
                    event.setLongitude(Double.valueOf(rawLocation.getString("lng")));
                    event.setLatitude(Double.valueOf(rawLocation.getString("lat")));
                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(jsonObjectRequest);
    }
}
