package ovh.bmyguest.bmyguest.fragments.Media;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.MediaListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Media;
import ovh.bmyguest.bmyguest.parcelables.User;

public class MediasFragment extends ListFragment {
    private static final String ARG_USER = "user";
    private static int PICK_MEDIA_REQUEST_CODE = 111;
    public MediaListAdapter mediasListAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<Media> mediasList = new ArrayList<>();
    public Uri pickedMediaUri;
    private User user;
    private MediaInteractionsListener listener;

    public static MediasFragment newInstance(User user) {
        MediasFragment fragment = new MediasFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medias, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(0);
            actionBar.setTitle(R.string.fragment_medias_title);
        }

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_medias);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listener.getMedias(user);
            }
        });

        mediasListAdapter = new MediaListAdapter(getActivity(), mediasList, 120);
        setListAdapter(mediasListAdapter);
        getMedias(user);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MediaInteractionsListener) {
            listener = (MediaInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement MediaInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onMediaSelected(mediasList, position);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_medias, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_createMedia:
                Intent pickMediaIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(pickMediaIntent, PICK_MEDIA_REQUEST_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_MEDIA_REQUEST_CODE) {
                pickedMediaUri = data.getData();
                Log.i("TEST PICK MEDIA", pickedMediaUri.toString());
            }
        }
    }

    public interface MediaInteractionsListener {
        void onMediaSelected(ArrayList<Media> mediasList, int position);
        void getMedias(User user);
    }

    public void getMedias(User user) {
        GsonRequest<Media[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_medias, user.getId()), Media[].class, new Response.Listener<Media[]>() {
            @Override
            public void onResponse(Media[] medias) {
                mediasList.clear();
                mediasList.addAll(Arrays.asList(medias));
                mediasListAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
