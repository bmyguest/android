package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.MediaListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Event;
import ovh.bmyguest.bmyguest.parcelables.Media;
import ovh.bmyguest.bmyguest.utils.Tools;

public class EventMediasFragment extends Fragment {
    private static final String ARG_EVENT = "event";
    public MediaListAdapter listAdapter;
    public ArrayList<Media> medias;
    private Event event;
    private MediaInteractionsListener listener;
    private int columnWidth;
    private GridView gridView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static EventMediasFragment newInstance(Event event) {
        EventMediasFragment fragment = new EventMediasFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_EVENT, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        event = getArguments() != null ? (Event) getArguments().getParcelable(ARG_EVENT) : null;
        medias = event != null ? event.getMedias() : new ArrayList<Media>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medias, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_medias);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMedias(event);
            }
        });

        gridView = view.findViewById(R.id.medias_gridview);
        initGridLayout();
        listAdapter = new MediaListAdapter(getActivity(), medias, columnWidth);
        gridView.setAdapter(listAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                listener.onMediaSelected(medias, position);
            }
        });

        getMedias(event);
    }

    private void initGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, r.getDisplayMetrics());
        columnWidth = (int) ((Tools.getScreenWidth(getContext()) - ((3 + 1) * padding)) / 3);
        gridView.setNumColumns(3);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MediaInteractionsListener) {
            listener = (MediaInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement MediaInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface MediaInteractionsListener {
        void onMediaSelected(ArrayList<Media> mediasList, int position);
    }

    public void getMedias(final Event event) {
        GsonRequest<Media[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_event_medias, event.getId()), Media[].class, new Response.Listener<Media[]>() {
            @Override
            public void onResponse(Media[] medias) {
                event.getMedias().clear();
                event.getMedias().addAll(Arrays.asList(medias));
                EventMediasFragment.this.medias.clear();
                EventMediasFragment.this.medias.addAll(Arrays.asList(medias));
                listAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
