package ovh.bmyguest.bmyguest.fragments.Evaluation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.text.SimpleDateFormat;
import java.util.Locale;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Evaluation;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;
import ovh.bmyguest.bmyguest.views.CircularNetworkImageView;

public class EvaluationFragment extends Fragment {
    private static final String ARG_USER = "user";
    private static final String ARG_EVALUATION = "evaluation";
    public Evaluation evaluation;
    private User user;

    public static EvaluationFragment newInstance(User user, Evaluation evaluation) {
        EvaluationFragment fragment = new EvaluationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_EVALUATION, evaluation);
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        evaluation = getArguments() != null ? (Evaluation) getArguments().getParcelable(ARG_EVALUATION) : null;
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_evaluation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(0);
            actionBar.setTitle(getString(R.string.fragment_evaluation_title));
        }

        displayEvaluation();
        getEvaluation(user, evaluation);
    }

    public void displayEvaluation() {
        if (getView() == null) {
            return;
        }

        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String updatedAtDate = formatDate.format(evaluation.getUpdatedAt());
        String updatedAtHour = formatHour.format(evaluation.getUpdatedAt());

        ((CircularNetworkImageView) getView().findViewById(R.id.evaluation_user_picture)).setImageUrl(evaluation.getUser().getPicture(), ApiRequestManager.getInstance(getActivity()).getImageLoader());
        ((TextView) getView().findViewById(R.id.evaluation_user_name)).setText(evaluation.getUser().getName());
        ((TextView) getView().findViewById(R.id.evaluation_date)).setText(getString(R.string.fragment_evaluation_date, updatedAtDate, updatedAtHour));
        ((TextView) getView().findViewById(R.id.evaluation_note)).setText(String.valueOf(evaluation.getNote()));
        ((TextView) getView().findViewById(R.id.evaluation_comment)).setText(Tools.decodeBase64String(evaluation.getComment()));
    }

    public void getEvaluation(User user, Evaluation eval) {
        GsonRequest<Evaluation> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_evaluation, user.getId(), eval.getId()), Evaluation.class, new Response.Listener<Evaluation>() {
            @Override
            public void onResponse(Evaluation response) {
                evaluation = response;
                displayEvaluation();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
