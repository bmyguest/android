package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.MediaListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Media;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;

public class ProfileMediasFragment extends Fragment {
    private static final String ARG_USER = "user";
    public MediaListAdapter mediasListAdapter;
    public ArrayList<Media> mediasList;
    private User user;
    private MediaInteractionsListener listener;
    private int columnWidth;
    private GridView gridView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static ProfileMediasFragment newInstance(User user) {
        ProfileMediasFragment fragment = new ProfileMediasFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        mediasList = user != null ? user.getMedias() : new ArrayList<Media>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medias, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_medias);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMedias(user);
            }
        });

        gridView = view.findViewById(R.id.medias_gridview);
        initGridLayout();
        mediasListAdapter = new MediaListAdapter(getActivity(), mediasList, columnWidth);
        gridView.setAdapter(mediasListAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                listener.onMediaSelected(mediasList, position);
            }
        });

        getMedias(user);
    }

    private void initGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, r.getDisplayMetrics());
        columnWidth = (int) ((Tools.getScreenWidth(getContext()) - ((3 + 1) * padding)) / 3);
        gridView.setNumColumns(3);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MediaInteractionsListener) {
            listener = (MediaInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement MediaInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface MediaInteractionsListener {
        void onMediaSelected(ArrayList<Media> mediasList, int position);
    }

    public void getMedias(final User user) {
        GsonRequest<Media[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_medias, user.getId()), Media[].class, new Response.Listener<Media[]>() {
            @Override
            public void onResponse(Media[] medias) {
                user.getMedias().clear();
                user.getMedias().addAll(Arrays.asList(medias));
                mediasList.clear();
                mediasList.addAll(Arrays.asList(medias));
                mediasListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
