package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.MemberListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Group;
import ovh.bmyguest.bmyguest.parcelables.User;

public class GroupMembersFragment extends ListFragment {
    private static final String ARG_GROUP = "group";
    public MemberListAdapter membersListAdapter;
    public ArrayList<User> membersList;
    private Group group;
    private MemberInteractionsListener listener;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static GroupMembersFragment newInstance(Group group) {
        GroupMembersFragment fragment = new GroupMembersFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GROUP, group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        group = getArguments() != null ? (Group) getArguments().getParcelable(ARG_GROUP) : null;
        membersList = group != null ? group.getMembers() : new ArrayList<User>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_members, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMembers(group);
            }
        });

        membersListAdapter = new MemberListAdapter(getActivity(), membersList);
        setListAdapter(membersListAdapter);
        getMembers(group);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MemberInteractionsListener) {
            listener = (MemberInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement MemberInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        User member = membersListAdapter.getItem(position);
        listener.onMemberSelected(member);
    }

    public interface MemberInteractionsListener {
        void onMemberSelected(User member);
    }

    public void getMembers(final Group group) {
        GsonRequest<User[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_groups_members, group.getId()), User[].class, new Response.Listener<User[]>() {
            @Override
            public void onResponse(User[] members) {
                group.getMembers().clear();
                group.getMembers().addAll(Arrays.asList(members));
                membersList.clear();
                membersList.addAll(Arrays.asList(members));
                membersListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
