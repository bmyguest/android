package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.ContactListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Group;
import ovh.bmyguest.bmyguest.parcelables.User;

public class ContactsFragment extends ListFragment implements SearchView.OnQueryTextListener {
    private static final String ARG_USER = "user";
    private static final int USER_BLOCKED = -1;
    public ContactListAdapter contactsListAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<User> contactsList = new ArrayList<>();
    private User user;
    private ContactInteractionsListener listener;
    private ActionBar actionBar;

    public static ContactsFragment newInstance(User user) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        contactsList = user != null ? user.getContacts() : new ArrayList<User>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);

            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                actionBar.setHomeAsUpIndicator(0);
            } else {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }

            actionBar.setTitle(getString(R.string.fragment_contacts_title, contactsList.size()));
        }

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_contacts);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContacts(user);
            }
        });

        registerForContextMenu(this.getListView());
        contactsListAdapter = new ContactListAdapter(getActivity(), contactsList);
        setListAdapter(contactsListAdapter);
        getContacts(user);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = this.getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.context_menu_fragment_contact, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        User contact = ((ContactListAdapter) getListAdapter()).getItem(info.position);

        switch (item.getItemId()) {
            case R.id.action_view_profile:
                listener.onContactSelected(contact);
                return true;
            case R.id.action_event_invite:
                listener.onContactInviteEvent(contact);
                return true;
            case R.id.action_group_invite:
                listener.onContactInviteGroup(contact);
                return true;
            case R.id.action_delete_contact:
                deleteContact(contact);
                return true;
            case R.id.action_block_contact:
                updateContact(contact, USER_BLOCKED);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ContactInteractionsListener) {
            listener = (ContactInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ContactInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        User contact = contactsListAdapter.getItem(position);
        listener.onContactSelected(contact);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_contacts, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        getContactsSuggestions(user, query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        getContactsSuggestions(user, newText);
        return true;
    }

    public interface ContactInteractionsListener {
        void onContactSelected(User contact);
        void onContactInviteEvent(User contact);
        void onContactInviteGroup(User contact);
    }

    private void getContactsSuggestions(User user, String query) {
        GsonRequest<User[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_contacts_suggestions, user.getId(), query), User[].class, new Response.Listener<User[]>() {
            @Override
            public void onResponse(User[] contacts) {
                contactsList.clear();
                contactsList.addAll(Arrays.asList(contacts));
                contactsListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    private void getContacts(final User user) {
        GsonRequest<User[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_contacts, user.getId()), User[].class, new Response.Listener<User[]>() {
            @Override
            public void onResponse(User[] contacts) {
                user.getContacts().clear();
                user.getContacts().addAll(Arrays.asList(contacts));
                contactsList.clear();
                contactsList.addAll(Arrays.asList(contacts));
                contactsListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                actionBar.setTitle(getString(R.string.fragment_contacts_title, contactsList.size()));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    private void updateContact(final User contact, int status) {
        HashMap<String, String> data = new HashMap<>();
        data.put("status", String.valueOf(status));

        GsonRequest<User> request = new GsonRequest<>(Request.Method.PUT, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_user_contact, user.getId(), contact.getId()), User.class, new Response.Listener<User>() {
            @Override
            public void onResponse(User response) {
                contactsList.remove(contact);
                contactsListAdapter.notifyDataSetChanged();
                actionBar.setTitle(getString(R.string.fragment_contacts_title, contactsList.size()));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    private void deleteContact(final User contact) {
        GsonRequest<Object> request = new GsonRequest<>(Request.Method.DELETE, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_contact, user.getId(), contact.getId()), Object.class, new Response.Listener<Object>() {
            @Override
            public void onResponse(Object response) {
                contactsList.remove(contact);
                contactsListAdapter.notifyDataSetChanged();
                actionBar.setTitle(getString(R.string.fragment_contacts_title, contactsList.size()));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
