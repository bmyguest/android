package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.network.MultiPartRequest;
import ovh.bmyguest.bmyguest.parcelables.Group;
import ovh.bmyguest.bmyguest.parcelables.Media;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;

import static android.app.Activity.RESULT_OK;

public class CreateGroupFragment extends Fragment {
    private static final String ARG_USER = "user";
    private static final int PICK_IMAGE_REQUEST = 6661;
    private User user;
    private Group group;
    private ImageView groupPicture;
    private EditText groupName;
    private EditText groupDescription;
    private Spinner groupIsOpen;
    private Spinner groupVisibility;
    private boolean isPosting = false;

    public static CreateGroupFragment newInstance(User user) {
        CreateGroupFragment fragment = new CreateGroupFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        group = new Group();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_group_create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_validate:
                if (!isPosting && checkInputValues()) {
                    isPosting = true;
                    setGroupValues();
                    uploadPictureAndCreateGroup();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_group_create, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(0);
            actionBar.setTitle(R.string.fragment_create_group_title);
        }

        displayForm();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                groupPicture.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void displayForm() {
        View view = getView();
        if (view == null) {
            return;
        }

        groupName = view.findViewById(R.id.group_name);
        groupDescription = view.findViewById(R.id.group_description);
        groupVisibility = view.findViewById(R.id.group_visibility);
        groupIsOpen = view.findViewById(R.id.group_is_open);
        groupPicture = view.findViewById(R.id.group_picture);
        groupPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
    }

    private boolean checkInputValues() {
        if (groupName.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid name", Toast.LENGTH_LONG).show();
            return false;
        }

        if (groupDescription.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Invalid description", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private void setGroupValues() {
        group.setName(groupName.getText().toString());
        group.setDescription(Tools.encodeBase64String(groupDescription.getText().toString()));
        group.setVisibility(groupVisibility.getSelectedItemPosition());
        group.setOpen(groupIsOpen.getSelectedItemPosition());
        group.setOwner(user);

        isPosting = false;
    }

    private void uploadPictureAndCreateGroup() {
        MultiPartRequest multipartRequest = new MultiPartRequest(Request.Method.POST, getString(R.string.general_api_url) + getString(R.string.general_api_url_medias_upload), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    Media media = new Gson().fromJson(json, Media.class);
                    group.setPicture(media);
                    createGroup(group);
                } catch (UnsupportedEncodingException | JsonSyntaxException error) {
                    Toast.makeText(getContext(), "Something went wrong with your picture :(", Toast.LENGTH_LONG).show();
                } finally {
                    isPosting = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
                isPosting = false;
            }
        }) {
            @Override
            public Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("picture", new DataPart("picture.jpg", Tools.getFileDataFromDrawable(groupPicture.getDrawable()), "image/jpeg"));
                return params;
            }
        };

        ApiRequestManager.getInstance(getContext()).getRequestQueue().add(multipartRequest);
    }

    public void createGroup(Group group) {
        HashMap<String, String> data = group.toHashMap();

        GsonRequest<Group> request = new GsonRequest<>(Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_groups), Group.class, new Response.Listener<Group>() {
            @Override
            public void onResponse(Group response) {
                getActivity().onBackPressed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
                isPosting = false;
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
