package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.GroupViewPagerAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Group;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;
import ovh.bmyguest.bmyguest.views.CircularNetworkImageView;

public class GroupFragment extends Fragment {
    private static final String ARG_GROUP = "group";
    public Group group;
    private GroupInteractionsListener listener;
    private ViewPager viewPager;
    private int currentPage = 0;
    private Button btnJoin;
    private Button btnManage;
    private Button btnLeave;
    private User loggedUser;

    public static GroupFragment newInstance(Group group) {
        GroupFragment fragment = new GroupFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GROUP, group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        group = getArguments() != null ? (Group) getArguments().getParcelable(ARG_GROUP) : null;
        loggedUser = ((MainActivity) getActivity()).user;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_group, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(0);
            actionBar.setTitle(group.getName());
        }

        displayGroup();
        listener.getGroup(group);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GroupInteractionsListener) {
            listener = (GroupInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement GroupInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void displayGroup() {
        View view = getView();
        if (view == null) {
            return;
        }

        CircularNetworkImageView ownerPicture = view.findViewById(R.id.group_owner_picture);
        ownerPicture.setImageUrl(group.getOwner().getPicture(), ApiRequestManager.getInstance(getActivity()).getImageLoader());
        ownerPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOwnerPictureClicked(group.getOwner());
            }
        });

        btnJoin = view.findViewById(R.id.btn_join);
        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                joinGroup(group);
            }
        });

        btnLeave = view.findViewById(R.id.btn_leave);
        btnLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leaveGroup(group);
            }
        });

        btnManage = view.findViewById(R.id.btn_manage);
        btnManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onManageGroup(group);
            }
        });

        updateButtons(loggedUser.isMember(group));

        ((NetworkImageView) view.findViewById(R.id.group_picture)).setImageUrl(group.getPicture().getFile(), ApiRequestManager.getInstance(getActivity()).getImageLoader());
        ((TextView) view.findViewById(R.id.group_name)).setText(group.getName());
        ((TextView) view.findViewById(R.id.group_description)).setText(Tools.decodeBase64String(group.getDescription()));
        ((TextView) view.findViewById(R.id.group_owner_username)).setText(getString(R.string.fragment_group_owner_name, group.getOwner().getName()));

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.removeAllTabs();
        tabLayout.addTab(tabLayout.newTab().setText("Medias"));
        tabLayout.addTab(tabLayout.newTab().setText("Members"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.group_viewpager);
        GroupViewPagerAdapter adapterViewPager = new GroupViewPagerAdapter(getActivity().getSupportFragmentManager(), group, tabLayout.getTabCount());
        viewPager.setAdapter(adapterViewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                if (viewPager.getCurrentItem() != 0) {
                    currentPage = viewPager.getCurrentItem();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setCurrentItem(currentPage);
    }

    public interface GroupInteractionsListener {
        void getGroup(Group group);
        void onOwnerPictureClicked(User user);
        void onManageGroup(Group group);
    }

    private void updateButtons(boolean isMember) {
        if (group.getOwner().getId().equals(loggedUser.getId())) {
            btnJoin.setVisibility(View.GONE);
            btnLeave.setVisibility(View.GONE);
            btnManage.setVisibility(View.VISIBLE);
        } else {
            btnJoin.setVisibility(isMember ? View.GONE : View.VISIBLE);
            btnLeave.setVisibility(isMember ? View.VISIBLE : View.GONE);
        }
    }

    public void joinGroup(final Group group) {
        HashMap<String, String> data = new HashMap<>();
        data.put("status", "0");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, getString(R.string.general_api_url) + getString(R.string.general_api_url_groups_members, group.getId()), new JSONObject(data), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                updateButtons(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", loggedUser.getToken());
                return headers;
            }
        };

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void leaveGroup(final Group group) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, getString(R.string.general_api_url) + getString(R.string.general_api_url_groups_members, group.getId()), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int count = 0;
                try {
                    count = response.getInt("count");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (count == 0) {
                    return;
                }

                updateButtons(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", loggedUser.getToken());
                return headers;
            }
        };

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
