package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.ContactListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.User;

public class ProfileContactsFragment extends ListFragment {
    private static final String ARG_USER = "user";
    public ContactListAdapter contactsListAdapter;
    public ArrayList<User> contactsList;
    private User user;
    private ContactInteractionsListener listener;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static ProfileContactsFragment newInstance(User user) {
        ProfileContactsFragment fragment = new ProfileContactsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        contactsList = user != null ? user.getContacts() : new ArrayList<User>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_contacts);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContacts(user);
            }
        });

        contactsListAdapter = new ContactListAdapter(getActivity(), contactsList);
        setListAdapter(contactsListAdapter);
        getContacts(user);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ContactInteractionsListener) {
            listener = (ContactInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ContactInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        User contact = contactsListAdapter.getItem(position);
        listener.onContactSelected(contact);
    }

    public interface ContactInteractionsListener {
        void onContactSelected(User contact);
    }

    public void getContacts(final User user) {
        GsonRequest<User[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_contacts, user.getId()), User[].class, new Response.Listener<User[]>() {
            @Override
            public void onResponse(User[] contacts) {
                user.getContacts().clear();
                user.getContacts().addAll(Arrays.asList(contacts));
                contactsList.clear();
                contactsList.addAll(Arrays.asList(contacts));
                contactsListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
