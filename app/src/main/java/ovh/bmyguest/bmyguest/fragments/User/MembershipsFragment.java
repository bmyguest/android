package ovh.bmyguest.bmyguest.fragments.User;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.bmyguest.MainActivity;
import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.adapters.GroupListAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Group;
import ovh.bmyguest.bmyguest.parcelables.User;

public class MembershipsFragment extends ListFragment implements SearchView.OnQueryTextListener {
    private static final String ARG_USER = "user";
    public GroupListAdapter groupsListAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<Group> groupsList;
    private User user;
    private GroupInteractionsListener listener;
    private ActionBar actionBar;

    public static MembershipsFragment newInstance(User user) {
        MembershipsFragment fragment = new MembershipsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        user = getArguments() != null ? (User) getArguments().getParcelable(ARG_USER) : null;
        groupsList = user != null ? user.getMemberships() : new ArrayList<Group>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_memberships, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);

            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                actionBar.setHomeAsUpIndicator(0);
            } else {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }

            actionBar.setTitle(getString(R.string.fragment_memberships_title, groupsList.size()));
        }

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMemberships(user);
            }
        });

        registerForContextMenu(this.getListView());
        groupsListAdapter = new GroupListAdapter(getActivity(), groupsList);
        setListAdapter(groupsListAdapter);
        getMemberships(user);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = this.getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.context_menu_fragment_memberships, menu);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Group group = ((GroupListAdapter) getListAdapter()).getItem(info.position);

        if (!group.getOwner().getId().equals(user.getId())) {
            menu.removeItem(R.id.action_manage_group);
        } else {
            menu.removeItem(R.id.action_leave_group);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Group group = ((GroupListAdapter) getListAdapter()).getItem(info.position);

        switch (item.getItemId()) {
            case R.id.action_manage_group:
                listener.onManageGroup(group);
                return true;
            case R.id.action_view_group:
                listener.onGroupSelected(group);
                return true;
            case R.id.action_invite_contacts:
                listener.onGroupInviteContact(group);
            case R.id.action_leave_group:
                leaveGroup(group);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GroupInteractionsListener) {
            listener = (GroupInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement GroupInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Group group = groupsListAdapter.getItem(position);
        listener.onGroupSelected(group);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_memberships, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        getMembershipsSuggestions(user, query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        getMembershipsSuggestions(user, newText);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                listener.onSearchClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public interface GroupInteractionsListener {
        void onSearchClick();
        void onGroupSelected(Group group);
        void onManageGroup(Group group);
        void onGroupInviteContact(Group group);
    }

    public void getMembershipsSuggestions(User user, String query) {
        GsonRequest<Group[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_memberships_suggestions, user.getId(), query), Group[].class, new Response.Listener<Group[]>() {
            @Override
            public void onResponse(Group[] groups) {
                groupsList.clear();
                groupsList.addAll(Arrays.asList(groups));
                groupsListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void getMemberships(User user) {
        GsonRequest<Group[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_memberships, user.getId()), Group[].class, new Response.Listener<Group[]>() {
            @Override
            public void onResponse(Group[] groups) {
                groupsList.clear();
                groupsList.addAll(Arrays.asList(groups));
                groupsListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                actionBar.setTitle(getString(R.string.fragment_memberships_title, groupsList.size()));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }

    public void leaveGroup(final Group group) {
        GsonRequest<JSONObject> request = new GsonRequest<>(Request.Method.DELETE, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_groups_members, group.getId()), JSONObject.class, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int count = 0;
                try {
                    count = response.getInt("count");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (count == 0) {
                    return;
                }

                groupsList.remove(group);
                groupsListAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(getActivity()).getRequestQueue().add(request);
    }
}
