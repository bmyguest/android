package ovh.bmyguest.bmyguest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ovh.bmyguest.bmyguest.fragments.Evaluation.EvaluationFragment;
import ovh.bmyguest.bmyguest.fragments.Evaluation.EvaluationsFragment;
import ovh.bmyguest.bmyguest.fragments.Event.CreateEventFragment;
import ovh.bmyguest.bmyguest.fragments.Event.EventFragment;
import ovh.bmyguest.bmyguest.fragments.Event.MyEventsFragment;
import ovh.bmyguest.bmyguest.fragments.Event.ParticipationsFragment;
import ovh.bmyguest.bmyguest.fragments.Media.GalleryFragment;
import ovh.bmyguest.bmyguest.fragments.Media.MediasFragment;
import ovh.bmyguest.bmyguest.fragments.Navigation.ExploreFragment;
import ovh.bmyguest.bmyguest.fragments.Navigation.SettingsFragment;
import ovh.bmyguest.bmyguest.fragments.User.ContactsFragment;
import ovh.bmyguest.bmyguest.fragments.User.CreateGroupFragment;
import ovh.bmyguest.bmyguest.fragments.User.EventEvaluationsFragment;
import ovh.bmyguest.bmyguest.fragments.User.EventMediasFragment;
import ovh.bmyguest.bmyguest.fragments.User.EventParticipantsFragment;
import ovh.bmyguest.bmyguest.fragments.User.GroupFragment;
import ovh.bmyguest.bmyguest.fragments.User.GroupMediasFragment;
import ovh.bmyguest.bmyguest.fragments.User.GroupMembersFragment;
import ovh.bmyguest.bmyguest.fragments.User.MembershipsFragment;
import ovh.bmyguest.bmyguest.fragments.User.MyGroupsFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileContactsFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileEvaluationsFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileMediasFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileMembershipsFragment;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.Evaluation;
import ovh.bmyguest.bmyguest.parcelables.Event;
import ovh.bmyguest.bmyguest.parcelables.Group;
import ovh.bmyguest.bmyguest.parcelables.Media;
import ovh.bmyguest.bmyguest.parcelables.User;

import static ovh.bmyguest.bmyguest.WelcomeActivity.EXTRA_USER;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        ContactsFragment.ContactInteractionsListener,
        MembershipsFragment.GroupInteractionsListener,
        EvaluationsFragment.EvaluationInteractionsListener,
        ParticipationsFragment.ParticipationInteractionsListener,
        ProfileFragment.ProfileInteractionsListener,
        EventFragment.EventInteractionsListener,
        GroupFragment.GroupInteractionsListener,
        MyEventsFragment.EventsInteractionListener,
        MediasFragment.MediaInteractionsListener,
        ProfileContactsFragment.ContactInteractionsListener,
        ProfileMembershipsFragment.GroupInteractionsListener,
        ProfileMediasFragment.MediaInteractionsListener,
        ProfileEvaluationsFragment.EvaluationInteractionsListener,
        MyGroupsFragment.MyGroupsInteractionListener,
        GroupMembersFragment.MemberInteractionsListener,
        GroupMediasFragment.MediaInteractionsListener,
        EventMediasFragment.MediaInteractionsListener,
        EventParticipantsFragment.ContactInteractionsListener,
        EventEvaluationsFragment.EvaluationInteractionsListener
{

    public ActionBar actionBar;
    public User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user = getIntent().getExtras() != null ? (User) getIntent().getExtras().getParcelable(EXTRA_USER) : null;

        if (user != null) {
            user.loadContacts(this);
            user.loadEvaluations(this);
            user.loadMedias(this);
            user.loadMemberships(this);
            user.loadParticipations(this);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        NetworkImageView navProfilePicture = headerView.findViewById(R.id.navigation_header_picture);
        navProfilePicture.setImageUrl(user.getPicture(), ApiRequestManager.getInstance(this).getImageLoader());
        navProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchFragment(ProfileFragment.newInstance(user), false);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        ((TextView) headerView.findViewById(R.id.navigation_header_name)).setText(user.getName());
        ((TextView) headerView.findViewById(R.id.navigation_header_location)).setText(user.getLocation());
        ((TextView) headerView.findViewById(R.id.navigation_header_note)).setText(String.valueOf(user.getNote()));

        onLogin();
    }

    @Override
    public void onMemberSelected(User member) {
        switchFragment(ProfileFragment.newInstance(member), true);
    }

    @Override
    public void onEvaluationSelected(Event event, Evaluation evaluation) {
        // TODO: show evaluation
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
    }

    public void onLogin() {
        switchFragment(new ExploreFragment(), false);
    }

    public void toggleDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                    closeKeyboard();
                } else {
                    toggleDrawer();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_explore:
                switchFragment(new ExploreFragment(), false);
                break;
//            case R.id.nav_now:
//                switchFragment(new ExploreFragment(), false);
//                break;
            case R.id.nav_profile:
                switchFragment(ProfileFragment.newInstance(user), false);
                break;
            case R.id.nav_contacts:
                switchFragment(ContactsFragment.newInstance(user), false);
                break;
            case R.id.nav_events:
                switchFragment(ParticipationsFragment.newInstance(user), false);
                break;
            case R.id.nav_groups:
                switchFragment(MembershipsFragment.newInstance(user), false);
                break;
            case R.id.nav_settings:
                switchFragment(new SettingsFragment(), false);
                break;
            case R.id.nav_help:
                break;
            case R.id.nav_my_events:
                switchFragment(MyEventsFragment.newInstance(user), false);
                break;
            case R.id.nav_my_groups:
                switchFragment(MyGroupsFragment.newInstance(user), false);
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void switchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        fragment.setRetainInstance(true);
        transaction.replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
        closeKeyboard();
    }

    public void copyToClipboard(String text) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getApplicationContext(), R.string.general_clipboard_copied, Toast.LENGTH_SHORT).show();
    }

    public void closeKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public void handleApiError(VolleyError error) {
        String body;
        if (error.networkResponse != null) {
            try {
                body = new String(error.networkResponse.data, "UTF-8");
                Toast.makeText(getApplicationContext(), "Error " + error.networkResponse.statusCode + ": " + body, Toast.LENGTH_SHORT).show();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onManageEvent(Event event) {
        // TODO: manage event
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    // EVENTS
    @Override
    public void onUserEventSelected(Event event) {
        switchFragment(EventFragment.newInstance(event), true);
    }

    @Override
    public void onCreateEventButtonPressed(User user) {
        switchFragment(CreateEventFragment.newInstance(user), true);
    }

    @Override
    public void onEventInviteContacts(Event event) {
        // TODO: invite contact to an event
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    @Override
    public void getEvent(Event event) {
        final EventFragment eventFragment = (EventFragment) getSupportFragmentManager().findFragmentByTag(EventFragment.class.getSimpleName());
        GsonRequest<Event> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_event, event.getId()), Event.class, new Response.Listener<Event>() {
            @Override
            public void onResponse(Event event) {
                eventFragment.event = event;
                eventFragment.displayEvent();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    // EVALUATIONS
    @Override
    public void onEvaluationSelected(User user, Evaluation evaluation) {
        switchFragment(EvaluationFragment.newInstance(user, evaluation), true);
    }

    @Override
    public void getEvaluations(User user) {
        final EvaluationsFragment evaluationsFragment = (EvaluationsFragment) getSupportFragmentManager().findFragmentByTag(EvaluationsFragment.class.getSimpleName());

        GsonRequest<Evaluation[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_evaluations, user.getId()), Evaluation[].class, new Response.Listener<Evaluation[]>() {
            @Override
            public void onResponse(Evaluation[] evaluations) {
                evaluationsFragment.evaluationsList.clear();
                evaluationsFragment.evaluationsList.addAll(Arrays.asList(evaluations));
                evaluationsFragment.evaluationsListAdapter.notifyDataSetChanged();
                evaluationsFragment.swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    // MEDIAS
    public void onMediaSelected(ArrayList<Media> mediasList, int position) {
        switchFragment(GalleryFragment.newInstance(mediasList, position), true);
    }

    public void getMedias(User user) {
        final MediasFragment mediasFragment = (MediasFragment) getSupportFragmentManager().findFragmentByTag(MediasFragment.class.getSimpleName());

        GsonRequest<Media[]> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user_medias, user.getId()), Media[].class, new Response.Listener<Media[]>() {
            @Override
            public void onResponse(Media[] medias) {
                mediasFragment.mediasList.clear();
                mediasFragment.mediasList.addAll(Arrays.asList(medias));
                mediasFragment.mediasListAdapter.notifyDataSetChanged();
                mediasFragment.swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onContactSelected(User contact) {
        switchFragment(ProfileFragment.newInstance(contact), true);
    }

    @Override
    public void onContactInviteEvent(User contact) {
        // TODO: invite contact to an event
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onContactInviteGroup(User contact) {
        // TODO: invite contact to a group
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSearchClick() {

    }

    @Override
    public void onManageGroup(Group group) {
        // TODO: manage group
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGroupInviteContact(Group group) {
        // TODO: invite contacts to group
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGroupSelected(Group group) {
        switchFragment(GroupFragment.newInstance(group), true);
    }

    @Override
    public void onCreateGroupButtonPressed(User user) {
        switchFragment(CreateGroupFragment.newInstance(user), true);
    }

    @Override
    public void getProfile(User user) {
        final ProfileFragment profileFragment = (ProfileFragment) getSupportFragmentManager().findFragmentByTag(ProfileFragment.class.getSimpleName());

        GsonRequest<User> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_user, user.getId()), User.class, new Response.Listener<User>() {
            @Override
            public void onResponse(User user) {
                profileFragment.user = user;
                profileFragment.displayProfile();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void getGroup(Group group) {
        final GroupFragment groupFragment = (GroupFragment) getSupportFragmentManager().findFragmentByTag(GroupFragment.class.getSimpleName());

        GsonRequest<Group> request = new GsonRequest<>(Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_group, group.getId()), Group.class, new Response.Listener<Group>() {
            @Override
            public void onResponse(Group group) {
                groupFragment.group = group;
                groupFragment.displayGroup();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onOwnerPictureClicked(User user) {
        switchFragment(ProfileFragment.newInstance(user), true);
    }

    @Override
    public void onManage(Event event) {
        // TODO: manage event
        Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
    }

    public void updateEvent(Event event) {
        HashMap<String, String> requestBody = event.toHashMap();
        final EventFragment eventFragment = (EventFragment) getSupportFragmentManager().findFragmentByTag(EventFragment.class.getSimpleName());
        String requestString = getString(R.string.general_api_url) + getString(R.string.general_api_url_event, event.getId());
        GsonRequest<Event> request = new GsonRequest<>(Request.Method.PUT, new JSONObject(requestBody), requestString, Event.class,
                new Response.Listener<Event>() {
                    @Override
                    public void onResponse(Event response) {
                        //TODO request's still throwing errors
                        eventFragment.event = response;
                        eventFragment.displayEvent();
                        onBackPressed();
                        closeKeyboard();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO why on success but still raising error

                handleApiError(error);
                onBackPressed();
                closeKeyboard();
            }
        });
        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }
}
