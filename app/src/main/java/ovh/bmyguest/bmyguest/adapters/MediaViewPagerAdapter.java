package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.Media;

public class MediaViewPagerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<Media> mediasList;

    public MediaViewPagerAdapter(Context context, ArrayList<Media> mediasList) {
        this.context = context;
        this.mediasList = mediasList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        NetworkImageView networkImageView = new NetworkImageView(context);
        networkImageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        networkImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        networkImageView.setImageUrl(mediasList.get(position).getFile(), ApiRequestManager.getInstance(context).getImageLoader());

        container.addView(networkImageView);
        return networkImageView;
    }

    @Override
    public int getCount() {
        return mediasList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
