package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.User;

public class ContactListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<User> contactsList;

    public ContactListAdapter(Context context, ArrayList<User> contactsList) {
        this.context = context;
        this.contactsList = contactsList;
    }

    public int getPosition(User contact) {
        return this.contactsList.indexOf(contact);
    }

    @Override
    public int getCount() {
        return contactsList.size();
    }

    @Override
    public User getItem(int position) {
        return contactsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_row_contact, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final User contact = getItem(position);
        viewHolder.contactName.setText(contact.getName());
        viewHolder.contactTags.setText(contact.getLocation());

        if (contact.getPicture() != null) {
            viewHolder.contactPicture.setImageUrl(contact.getPicture(), ApiRequestManager.getInstance(context).getImageLoader());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView contactName;
        TextView contactTags;
        NetworkImageView contactPicture;

        ViewHolder(View view) {
            contactName = view.findViewById(R.id.text_view_contact_name);
            contactTags = view.findViewById(R.id.text_view_contact_tags);
            contactPicture = view.findViewById(R.id.image_view_contact_picture);
        }
    }
}
