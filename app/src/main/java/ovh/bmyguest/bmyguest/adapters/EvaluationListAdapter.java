package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.Evaluation;
import ovh.bmyguest.bmyguest.utils.Tools;

public class EvaluationListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Evaluation> evaluationsList;

    public EvaluationListAdapter(Context context, ArrayList<Evaluation> evaluationsList) {
        this.context = context;
        this.evaluationsList = evaluationsList;
    }

    public int getPosition(Evaluation evaluation) {
        return this.evaluationsList.indexOf(evaluation);
    }

    @Override
    public int getCount() {
        return evaluationsList.size();
    }

    @Override
    public Object getItem(int position) {
        return evaluationsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_row_evaluation, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Evaluation evaluation = (Evaluation) getItem(position);
        viewHolder.evaluationComment.setText(Tools.decodeBase64String(evaluation.getComment()));
        viewHolder.evaluationNote.setText(String.valueOf(evaluation.getNote()));

        if (evaluation.getUser() != null) {
            viewHolder.userName.setText(evaluation.getUser().getName());
            viewHolder.userPicture.setImageUrl(evaluation.getUser().getPicture(), ApiRequestManager.getInstance(context).getImageLoader());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView evaluationComment;
        TextView evaluationNote;
        TextView userName;
        NetworkImageView userPicture;

        ViewHolder(View view) {
            evaluationComment = view.findViewById(R.id.text_view_evaluation_comment);
            evaluationNote = view.findViewById(R.id.text_view_evaluation_note);
            userName = view.findViewById(R.id.text_view_evaluation_user_name);
            userPicture = view.findViewById(R.id.image_view_evaluation_user_picture);
        }
    }
}
