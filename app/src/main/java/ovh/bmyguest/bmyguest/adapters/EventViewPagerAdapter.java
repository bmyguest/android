package ovh.bmyguest.bmyguest.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ovh.bmyguest.bmyguest.fragments.User.EventEvaluationsFragment;
import ovh.bmyguest.bmyguest.fragments.User.EventMediasFragment;
import ovh.bmyguest.bmyguest.fragments.User.EventParticipantsFragment;
import ovh.bmyguest.bmyguest.parcelables.Event;

public class EventViewPagerAdapter extends FragmentStatePagerAdapter {
    private Event event;
    private int tabsNumber;

    public EventViewPagerAdapter(FragmentManager fragmentManager, Event event, int tabsNumber) {
        super(fragmentManager);
        this.event = event;
        this.tabsNumber = tabsNumber;
    }

    @Override
    public int getCount() {
        return tabsNumber;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return EventMediasFragment.newInstance(event);
            case 1:
                return EventParticipantsFragment.newInstance(event);
            case 2:
                return EventEvaluationsFragment.newInstance(event);
            default:
                return null;
        }
    }
}
