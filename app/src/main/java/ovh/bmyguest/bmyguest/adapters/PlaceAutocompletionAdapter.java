package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.parcelables.CustomPlace;

public class PlaceAutocompletionAdapter extends ArrayAdapter<CustomPlace> {

    public ArrayList<CustomPlace> places;
    private int layoutId;
    Context context;
    LayoutInflater layoutInflater;

    public PlaceAutocompletionAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<CustomPlace> places) {
        super(context, resource);
        this.layoutId = resource;
        this.context = context;
        this.places = places;
        this.layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return places.size();
    }

    @Nullable
    @Override
    public CustomPlace getItem(int position) {
        return places.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        if (view == null){
            view = layoutInflater.inflate(layoutId, null);
        }
        CustomPlace customPlace = getItem(i);

        if (customPlace != null) {
            TextView placeDescriptTextView = view.findViewById(R.id.row_place_autocompletion_textView);
            placeDescriptTextView.setText(customPlace.getDescription());
        }

        return view;
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return placeFilter;
    }

    Filter placeFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((CustomPlace) resultValue).getDescription();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();
            if(charSequence != null){
                filterResults.values = places;
                filterResults.count = places.size();
            }
            return filterResults;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if(filterResults != null && filterResults.count > 0){
                clear();
                addAll((ArrayList<CustomPlace>) filterResults.values);
            }else{
                addAll(places);
            }
            notifyDataSetChanged();
        }
    };

}
