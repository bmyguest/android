package ovh.bmyguest.bmyguest.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ovh.bmyguest.bmyguest.fragments.User.GroupMediasFragment;
import ovh.bmyguest.bmyguest.fragments.User.GroupMembersFragment;
import ovh.bmyguest.bmyguest.parcelables.Group;

public class GroupViewPagerAdapter extends FragmentStatePagerAdapter {
    private Group group;
    private int tabsNumber;

    public GroupViewPagerAdapter(FragmentManager fragmentManager, Group group, int tabsNumber) {
        super(fragmentManager);
        this.group = group;
        this.tabsNumber = tabsNumber;
    }

    @Override
    public int getCount() {
        return tabsNumber;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return GroupMediasFragment.newInstance(group);
            case 1:
                return GroupMembersFragment.newInstance(group);
            default:
                return null;
        }
    }
}
