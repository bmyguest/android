package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.Media;

public class MediaListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Media> mediasList = new ArrayList<>();
    private int imageWidth;

    public MediaListAdapter(Context context, ArrayList<Media> mediasList, int imageWidth) {
        this.context = context;
        this.mediasList = mediasList;
        this.imageWidth = imageWidth;
    }

    public int getPosition(Media media) {
        return mediasList.indexOf(media);
    }

    @Override
    public int getCount() {
        return mediasList.size();
    }

    @Override
    public Media getItem(int position) {
        return mediasList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mediasList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NetworkImageView networkImageView;

        if (convertView == null) {
            networkImageView = new NetworkImageView(context);
            networkImageView.setLayoutParams(new GridView.LayoutParams(imageWidth, imageWidth));
            networkImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            networkImageView = (NetworkImageView) convertView;
        }

        networkImageView.setImageUrl(mediasList.get(position).getFile(),  ApiRequestManager.getInstance(context).getImageLoader());
        return networkImageView;
    }
}
