package ovh.bmyguest.bmyguest.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ovh.bmyguest.bmyguest.fragments.User.ProfileContactsFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileEvaluationsFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileMediasFragment;
import ovh.bmyguest.bmyguest.fragments.User.ProfileMembershipsFragment;
import ovh.bmyguest.bmyguest.parcelables.User;

public class ProfileViewPagerAdapter extends FragmentStatePagerAdapter {
    private User user;
    private int tabsNumber;

    public ProfileViewPagerAdapter(FragmentManager fragmentManager, User user, int tabsNumber) {
        super(fragmentManager);
        this.user = user;
        this.tabsNumber = tabsNumber;
    }

    @Override
    public int getCount() {
        return tabsNumber;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ProfileContactsFragment.newInstance(user);
            case 1:
                return ProfileMediasFragment.newInstance(user);
            case 2:
                return ProfileMembershipsFragment.newInstance(user);
            case 3:
                return ProfileEvaluationsFragment.newInstance(user);
            default:
                return null;
        }
    }
}
