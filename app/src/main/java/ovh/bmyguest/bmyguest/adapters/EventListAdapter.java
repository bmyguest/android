package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.Event;

public class EventListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Event> eventsList;

    public EventListAdapter(Context context, ArrayList<Event> eventsList) {
        this.context = context;
        this.eventsList = eventsList;

    }

    public int getPosition(Event event) {
        return this.eventsList.indexOf(event);
    }

    @Override
    public int getCount() {
        return eventsList.size();
    }

    @Override
    public Event getItem(int position) {
        return eventsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_row_event, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Event event = getItem(position);
        SimpleDateFormat dfpicture = new SimpleDateFormat("MMM.\ndd", Locale.getDefault());
        SimpleDateFormat df = new SimpleDateFormat("MMM. dd HH:mm", Locale.getDefault());
        String pictureText = dfpicture.format(event.getStart());
        String startDateText = df.format(event.getStart());
        String endDateText = df.format(event.getEnd());

        viewHolder.eventName.setText(event.getName());
        viewHolder.eventDate.setText(startDateText + " - " + endDateText);
        viewHolder.eventPictureDate.setText(pictureText);
        viewHolder.eventLocation.setText(event.getAddress());
        viewHolder.eventParticipants.setText(context.getResources().getQuantityString(R.plurals.fragment_events_participants, event.getParticipants().size(), event.getParticipants().size()));

        if (event.getPicture() != null) {
            viewHolder.eventPicture.setImageUrl(event.getPicture().getFile(), ApiRequestManager.getInstance(context).getImageLoader());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView eventName;
        TextView eventDate;
        TextView eventPictureDate;
        TextView eventLocation;
        TextView eventParticipants;
        NetworkImageView eventPicture;

        ViewHolder(View view) {
            eventName = view.findViewById(R.id.text_view_event_name);
            eventDate = view.findViewById(R.id.text_view_event_date);
            eventPictureDate = view.findViewById(R.id.text_view_event_picture_date);
            eventLocation = view.findViewById(R.id.text_view_event_location);
            eventParticipants = view.findViewById(R.id.text_view_event_participants);
            eventPicture = view.findViewById(R.id.image_view_event_picture);
        }
    }
}
