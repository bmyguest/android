package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.Group;

public class GroupListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Group> groupsList;

    public GroupListAdapter(Context context, ArrayList<Group> groupsList) {
        this.context = context;
        this.groupsList = groupsList;
    }

    public int getPosition(Group group) {
        return this.groupsList.indexOf(group);
    }

    @Override
    public int getCount() {
        return groupsList.size();
    }

    @Override
    public Group getItem(int position) {
        return groupsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_row_group, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Group group = getItem(position);
        viewHolder.groupName.setText(group.toString());
        viewHolder.groupMembers.setText(context.getResources().getQuantityString(R.plurals.fragment_groups_members, group.getMembers().size(), group.getMembers().size()));

        if (group.getPicture() != null) {
            viewHolder.groupPicture.setImageUrl(group.getPicture().getFile(), ApiRequestManager.getInstance(context).getImageLoader());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView groupName;
        NetworkImageView groupPicture;
        TextView groupMembers;

        ViewHolder(View view) {
            groupName = view.findViewById(R.id.text_view_group_name);
            groupPicture = view.findViewById(R.id.image_view_group_picture);
            groupMembers = view.findViewById(R.id.text_view_group_members);
        }
    }
}
