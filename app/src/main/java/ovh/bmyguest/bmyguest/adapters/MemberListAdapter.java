package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.User;

public class MemberListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<User> membersList;

    public MemberListAdapter(Context context, ArrayList<User> membersList) {
        this.context = context;
        this.membersList = membersList;
    }

    public int getPosition(User member) {
        return this.membersList.indexOf(member);
    }

    @Override
    public int getCount() {
        return membersList.size();
    }

    @Override
    public User getItem(int position) {
        return membersList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_row_member, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final User member = getItem(position);
        viewHolder.memberName.setText(member.getName());
        viewHolder.memberTags.setText(member.getLocation());

        if (member.getPicture() != null) {
            viewHolder.memberPicture.setImageUrl(member.getPicture(), ApiRequestManager.getInstance(context).getImageLoader());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView memberName;
        TextView memberTags;
        NetworkImageView memberPicture;

        ViewHolder(View view) {
            memberName = view.findViewById(R.id.text_view_name);
            memberTags = view.findViewById(R.id.text_view_tags);
            memberPicture = view.findViewById(R.id.image_view_picture);
        }
    }
}
