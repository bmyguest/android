package ovh.bmyguest.bmyguest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.parcelables.Suggestion;

public class SuggestionListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Suggestion> suggestionsList;

    public SuggestionListAdapter(Context context, ArrayList<Suggestion> suggestionsList) {
        this.context = context;
        this.suggestionsList = suggestionsList;
    }

    public int getPosition(Suggestion suggestion) {
        return this.suggestionsList.indexOf(suggestion);
    }

    @Override
    public int getCount() {
        return suggestionsList.size();
    }

    @Override
    public Object getItem(int position) {
        return suggestionsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_row_suggestion, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Suggestion suggestion = (Suggestion) getItem(position);
        viewHolder.suggestionName.setText(suggestion.toString());
        viewHolder.suggestionDescription.setText(suggestion.getDescription());

        if (suggestion.getPicture() != null) {
            viewHolder.suggestionPicture.setImageUrl(suggestion.getPicture().getFile(), ApiRequestManager.getInstance(context).getImageLoader());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView suggestionName;
        TextView suggestionDescription;
        NetworkImageView suggestionPicture;

        ViewHolder(View view) {
            suggestionName = view.findViewById(R.id.text_view_suggestion_name);
            suggestionDescription = view.findViewById(R.id.text_view_suggestion_description);
            suggestionPicture = view.findViewById(R.id.image_view_suggestion_picture);
        }
    }
}
