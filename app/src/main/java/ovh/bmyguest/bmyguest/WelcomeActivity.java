package ovh.bmyguest.bmyguest;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import ovh.bmyguest.bmyguest.adapters.IntroViewPagerAdapter;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;
import ovh.bmyguest.bmyguest.parcelables.User;
import ovh.bmyguest.bmyguest.utils.Tools;

public class WelcomeActivity extends AppCompatActivity implements
        FacebookCallback<LoginResult>,
        ViewPager.OnPageChangeListener,
        View.OnClickListener
{
    private ViewPager viewPager;
    private LinearLayout dotsLayout;
    private int[] layouts;
    private Button btnNext;
    private Activity activity;
    private CallbackManager callbackManager;
    public static final String EXTRA_USER = "EXTRA_USER";
    private static final int PERMISSION_LOCATION_GRANTED = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = this;
        setContentView(R.layout.activity_loading);

        Tools.printDeviceHash(this);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        Profile profile = Profile.getCurrentProfile();

        if (accessToken != null && !accessToken.isExpired() && profile != null) {
            getUserFromToken(accessToken);
            return;
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_welcome);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnNext = (Button) findViewById(R.id.btn_next);

        layouts = new int[] {
            R.layout.welcome_slide1,
            R.layout.welcome_slide2,
            R.layout.welcome_slide3,
            R.layout.welcome_slide4
        };

        addBottomDots(0);
        changeStatusBarColor();

        IntroViewPagerAdapter viewPagerAdapter = new IntroViewPagerAdapter(this, layouts);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        getUserFromToken(loginResult.getAccessToken());
    }

    public void getUserFromToken(final AccessToken token) {
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,gender,birthday,link,email,picture.type(large),location,events,friends");

        GraphRequest request = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String id = object.getString("id");
                    String accessToken = token.getToken();
                    String name = object.getString("name");
                    String email = object.has("email") ? object.getString("email") : null;
                    String gender = object.getString("gender");
                    String location = object.has("location") ? object.getJSONObject("location") != null ? object.getJSONObject("location").getString("name") : null : null;
                    String picture = object.getJSONObject("picture") != null ? object.getJSONObject("picture").getJSONObject("data") != null ? object.getJSONObject("picture").getJSONObject("data").getString("url") : null : null;
                    String birthday =  object.has("birthday") ? object.getString("birthday") : null;

                    User user = new User(getApplicationContext(), id, accessToken, name, email, birthday, gender, location, picture, false, 7);
                    upsertUser(user);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onCancel() {
        Toast.makeText(activity, getString(R.string.facebook_cancel_text), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(FacebookException exception) {
        Toast.makeText(activity, getString(R.string.facebook_error_text), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_LOCATION_GRANTED:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnNext.setVisibility(View.VISIBLE);
                } else {
                    btnNext.setVisibility(View.GONE);
                }

                break;
        }
    }

    @Override
    public void onPageSelected(int position) {
        addBottomDots(position);

        if (position == layouts.length - 1) {
            btnNext.setVisibility(View.GONE);
            callbackManager = CallbackManager.Factory.create();
            LoginButton facebookLoginButton = (LoginButton) findViewById(R.id.facebook_login_button);
            facebookLoginButton.setReadPermissions("email", "user_birthday", "user_location", "user_events", "user_friends");
            facebookLoginButton.registerCallback(callbackManager, this);
        } else if (position == layouts.length - 2) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                btnNext.setVisibility(View.GONE);
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION,}, PERMISSION_LOCATION_GRANTED);
            } else {
                btnNext.setVisibility(View.VISIBLE);
            }
        } else {
            btnNext.setText(getString(R.string.next));
        }
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onClick(View v) {
        int current = viewPager.getCurrentItem() + 1;
        viewPager.setCurrentItem(current);
    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[layouts.length];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[currentPage].setTextColor(colorsActive[currentPage]);
        }
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void upsertUser(User user) {
        HashMap<String, String> data = user.toHashMap();

        GsonRequest<User> request = new GsonRequest<>(Request.Method.POST, new JSONObject(data), getResources().getString(R.string.general_api_url) + getResources().getString(R.string.general_api_url_users), User.class, new Response.Listener<User>() {
            @Override
            public void onResponse(User response) {
                if (response != null){
                    Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                    intent.putExtra(EXTRA_USER, response);
                    startActivity(intent);
                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    public void handleApiError(VolleyError error) {
        String body;
        if (error.networkResponse != null) {
            try {
                body = new String(error.networkResponse.data, "UTF-8");
                Toast.makeText(getApplicationContext(), "Error " + error.networkResponse.statusCode + ": " + body, Toast.LENGTH_SHORT).show();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
