package ovh.bmyguest.bmyguest.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

public class MediaCategory implements Parcelable {
    public static final Creator<MediaCategory> CREATOR = new Creator<MediaCategory>() {
        @Override
        public MediaCategory createFromParcel(Parcel in) {
            return new MediaCategory(in);
        }

        @Override
        public MediaCategory[] newArray(int size) {
            return new MediaCategory[size];
        }
    };
    private int id;
    private String name;

    private MediaCategory(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public MediaCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}