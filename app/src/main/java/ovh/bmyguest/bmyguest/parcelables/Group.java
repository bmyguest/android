package ovh.bmyguest.bmyguest.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

public class Group implements Parcelable {
    private int id;
    private String name;
    private String description;
    private int open;
    private int visibility;
    private Media picture;
    private User owner;
    private ArrayList<User> members = new ArrayList<>();
    private ArrayList<Media> medias = new ArrayList<>();
    private ArrayList<Tag> tags = new ArrayList<>();

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    public Group() {

    }

    private Group(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        open = in.readInt();
        visibility = in.readInt();
        picture = in.readParcelable(getClass().getClassLoader());
        owner = in.readParcelable(getClass().getClassLoader());
        in.readTypedList(tags, Tag.CREATOR);
        in.readTypedList(members, User.CREATOR);
        in.readTypedList(medias, Media.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(description);
        out.writeInt(open);
        out.writeInt(visibility);
        out.writeParcelable(picture, flags);
        out.writeParcelable(owner, flags);
        out.writeTypedList(tags);
        out.writeTypedList(members);
        out.writeTypedList(medias);
    }

    public HashMap<String, String> toHashMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", this.name);
        hashMap.put("description", this.description);
        hashMap.put("visibility", String.valueOf(this.visibility));
        hashMap.put("open", String.valueOf(this.open));
        hashMap.put("pictureId", String.valueOf(this.picture.getId()));

        return hashMap;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getOpen() {
        return open;
    }

    public int getVisibility() {
        return visibility;
    }

    public Media getPicture() {
        return picture;
    }

    public User getOwner() {
        return owner;
    }

    public ArrayList<User> getMembers() {
        return members;
    }

    public ArrayList<Media> getMedias() {
        return medias;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public void setPicture(Media picture) {
        this.picture = picture;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}