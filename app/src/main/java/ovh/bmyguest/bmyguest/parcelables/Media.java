package ovh.bmyguest.bmyguest.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

public class Media implements Parcelable {
    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };
    private int id;
    private String file;
    private int visibility;

    private Media(Parcel in) {
        id = in.readInt();
        file = in.readString();
        visibility = in.readInt();
    }

    public Media(int id, String file, int visibility) {
        this.id = id;
        this.file = file;
        this.visibility = visibility;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(file);
        out.writeInt(visibility);
    }

    @Override
    public String toString() {
        return file;
    }

    public int getId() {
        return id;
    }

    public String getFile() {
        return file;
    }

    public int getVisibility() {
        return visibility;
    }
}