package ovh.bmyguest.bmyguest.parcelables;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import ovh.bmyguest.bmyguest.R;
import ovh.bmyguest.bmyguest.network.ApiRequestManager;
import ovh.bmyguest.bmyguest.network.GsonRequest;

public class User implements Parcelable {
    private String id;
    private String token;
    private String name;
    private String email;
    private String birthday;
    private String gender;
    private String location;
    private String picture;
    private boolean isPremium;
    private double note;
    private ArrayList<Tag> tags = new ArrayList<>();
    private ArrayList<Evaluation> evaluations = new ArrayList<>();
    private ArrayList<Media> medias = new ArrayList<>();
    private ArrayList<User> contacts = new ArrayList<>();
    private ArrayList<Event> events = new ArrayList<>();
    private ArrayList<Group> memberships = new ArrayList<>();
    private ArrayList<Event> participations = new ArrayList<>();

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    private User(Parcel in) {
        id = in.readString();
        token = in.readString();
        name = in.readString();
        email = in.readString();
        birthday = in.readString();
        gender = in.readString();
        location = in.readString();
        picture = in.readString();
        isPremium = in.readInt() == 1;
        note = in.readDouble();
        in.readTypedList(tags, Tag.CREATOR);
        in.readTypedList(evaluations, Evaluation.CREATOR);
        in.readTypedList(medias, Media.CREATOR);
        in.readTypedList(contacts, User.CREATOR);
        in.readTypedList(events, Event.CREATOR);
        in.readTypedList(memberships, Group.CREATOR);
        in.readTypedList(participations, Event.CREATOR);
    }

    public User(Context context, String id, String token, String name, String email, String birthday, String gender, String location, String picture, boolean isPremium, double note) {
        this.id = id;
        this.token = token;
        this.name = name;
        this.email = email;
        this.birthday = birthday;
        this.gender = gender;
        this.location = location;
        this.isPremium = isPremium;
        this.note = note;
        this.picture = picture;
    }

    public User() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(token);
        out.writeString(name);
        out.writeString(email);
        out.writeString(birthday);
        out.writeString(gender);
        out.writeString(location);
        out.writeString(picture);
        out.writeInt(isPremium ? 1 : 0);
        out.writeDouble(note);
        out.writeTypedList(tags);
        out.writeTypedList(evaluations);
        out.writeTypedList(medias);
        out.writeTypedList(contacts);
        out.writeTypedList(events);
        out.writeTypedList(memberships);
        out.writeTypedList(participations);
    }

    public HashMap<String, String> toHashMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", this.id);
        hashMap.put("token", this.token);
        hashMap.put("name", this.name);
        hashMap.put("email", this.email);
        hashMap.put("birthday", this.birthday);
        hashMap.put("gender", this.gender);
        hashMap.put("location", this.location);
        hashMap.put("picture", this.picture);

        return hashMap;
    }

    public String getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthday() {
        return birthday;
    }

    public int getAge() {
        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();
        int age;
        Date birthday = null;

        try {
            birthday = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).parse(this.birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        birthDate.setTime(birthday);
        if (birthDate.after(today)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }

        age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

        if ((birthDate.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR) > 3) || (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH ))) {
            age--;
        } else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH )) && (birthDate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH ))) {
            age--;
        }

        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getLocation() {
        return location;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public double getNote() {
        return note;
    }

    public String getPicture() {
        return picture;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public ArrayList<Evaluation> getEvaluations() {
        return evaluations;
    }

    public ArrayList<Media> getMedias() {
        return medias;
    }

    public ArrayList<User> getContacts() {
        return contacts;
    }

    public ArrayList<Group> getMemberships() {
        return memberships;
    }

    public ArrayList<Event> getParticipations() {
        return participations;
    }

    public ArrayList<Event> getUserEvents() {
        return events;
    }

    public boolean isMember(Group group) {
        for (Group membership : this.memberships) {
            if (membership.getId() == group.getId()) {
                return true;
            }
        }

        return false;
    }

    public boolean isParticipant(Event event) {
        for (Event participation : this.participations) {
            if (participation.getId() == event.getId()) {
                return true;
            }
        }

        return false;
    }

    public void loadContacts(Context context) {
        GsonRequest<User[]> request = new GsonRequest<>(Request.Method.GET, null, context.getString(R.string.general_api_url) + context.getString(R.string.general_api_url_user_contacts, this.id), User[].class, new Response.Listener<User[]>() {
            @Override
            public void onResponse(User[] response) {
                contacts.clear();
                contacts.addAll(Arrays.asList(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ApiRequestManager.getInstance(context).getRequestQueue().add(request);
    }

    public void loadEvaluations(Context context) {
        GsonRequest<Evaluation[]> request = new GsonRequest<>(Request.Method.GET, null, context.getString(R.string.general_api_url) + context.getString(R.string.general_api_url_user_evaluations, this.id), Evaluation[].class, new Response.Listener<Evaluation[]>() {
            @Override
            public void onResponse(Evaluation[] response) {
                evaluations.clear();
                evaluations.addAll(Arrays.asList(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ApiRequestManager.getInstance(context).getRequestQueue().add(request);
    }

    public void loadMedias(Context context) {
        GsonRequest<Media[]> request = new GsonRequest<>(Request.Method.GET, null, context.getString(R.string.general_api_url) + context.getString(R.string.general_api_url_user_medias, this.id), Media[].class, new Response.Listener<Media[]>() {
            @Override
            public void onResponse(Media[] response) {
                medias.clear();
                medias.addAll(Arrays.asList(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ApiRequestManager.getInstance(context).getRequestQueue().add(request);
    }

    public void loadMemberships(Context context) {
        GsonRequest<Group[]> request = new GsonRequest<>(Request.Method.GET, null, context.getString(R.string.general_api_url) + context.getString(R.string.general_api_url_user_memberships, this.id), Group[].class, new Response.Listener<Group[]>() {
            @Override
            public void onResponse(Group[] response) {
                memberships.clear();
                memberships.addAll(Arrays.asList(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ApiRequestManager.getInstance(context).getRequestQueue().add(request);
    }

    public void loadParticipations(Context context) {
        GsonRequest<Event[]> request = new GsonRequest<>(Request.Method.GET, null, context.getString(R.string.general_api_url) + context.getString(R.string.general_api_url_user_participations, this.id), Event[].class, new Response.Listener<Event[]>() {
            @Override
            public void onResponse(Event[] response) {
                participations.clear();
                participations.addAll(Arrays.asList(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ApiRequestManager.getInstance(context).getRequestQueue().add(request);
    }
}