package ovh.bmyguest.bmyguest.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Evaluation implements Parcelable {
    private int id;
    private int note;
    private String comment;
    private Date updatedAt;
    private User user;

    public static final Creator<Evaluation> CREATOR = new Creator<Evaluation>() {
        @Override
        public Evaluation createFromParcel(Parcel in) {
            return new Evaluation(in);
        }

        @Override
        public Evaluation[] newArray(int size) {
            return new Evaluation[size];
        }
    };

    private Evaluation(Parcel in) {
        id = in.readInt();
        note = in.readInt();
        comment = in.readString();
        updatedAt = new Date(in.readLong());
        user = in.readParcelable(getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeInt(note);
        out.writeString(comment);
        out.writeLong(updatedAt.getTime());
        out.writeParcelable(user, flags);
    }

    @Override
    public String toString() {
        return String.valueOf(note);
    }

    public int getId() {
        return id;
    }

    public int getNote() {
        return note;
    }

    public String getComment() {
        return this.comment;
    }

    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    public User getUser() {
        return user;
    }
}