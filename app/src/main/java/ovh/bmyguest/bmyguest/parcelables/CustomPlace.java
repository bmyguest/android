package ovh.bmyguest.bmyguest.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomPlace implements Parcelable {
    private String id;
    private String description;
    private double longitude;
    private double latitude;

    public static final Creator<CustomPlace> CREATOR = new Creator<CustomPlace>() {
        @Override
        public CustomPlace createFromParcel(Parcel in) {
            return new CustomPlace(in);
        }

        @Override
        public CustomPlace[] newArray(int size) {
            return new CustomPlace[size];
        }
    };

    public CustomPlace(String id, String description) {
        this.id = id;
        this.description = description;
    }

    private CustomPlace(Parcel in) {
        id = in.readString();
        description = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String toString(){
        return this.description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(description);
        out.writeDouble(latitude);
        out.writeDouble(longitude);
    }
}
