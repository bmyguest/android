package ovh.bmyguest.bmyguest.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Event implements Parcelable {
    private int id;
    private String name;
    private String description;
    private Date start = new Date();
    private Date end = new Date();
    private double latitude;
    private double longitude;
    private int capacity;
    private int recurrence;
    private int visibility;
    private Media picture;
    private User owner;
    private ArrayList<User> participants = new ArrayList<>();
    private ArrayList<Media> medias = new ArrayList<>();
    private ArrayList<Evaluation> evaluations = new ArrayList<>();
    private ArrayList<Tag> tags = new ArrayList<>();
    private String address;

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    private Event(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        start = new Date(in.readLong());
        end = new Date(in.readLong());
        latitude = in.readDouble();
        longitude = in.readDouble();
        capacity = in.readInt();
        recurrence = in.readInt();
        visibility = in.readInt();
        address = in.readString();
        picture = in.readParcelable(Media.class.getClassLoader());
        owner = in.readParcelable(User.class.getClassLoader());
        in.readTypedList(tags, Tag.CREATOR);
        in.readTypedList(evaluations, Evaluation.CREATOR);
        in.readTypedList(medias, Media.CREATOR);
        in.readTypedList(participants, User.CREATOR);
    }

    public Event(int id, String name, String description, Date start, Date end, double latitude, double longitude, int capacity, int recurrence, int visibility, Media picture, User owner) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.start = start;
        this.end = end;
        this.latitude = latitude;
        this.longitude = longitude;
        this.capacity = capacity;
        this.recurrence = recurrence;
        this.visibility = visibility;
        this.picture = picture;
        this.owner = owner;
    }

    public Event() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(description);
        out.writeLong(start.getTime());
        out.writeLong(end.getTime());
        out.writeDouble(latitude);
        out.writeDouble(longitude);
        out.writeInt(capacity);
        out.writeInt(recurrence);
        out.writeInt(visibility);
        out.writeParcelable(picture, flags);
        out.writeParcelable(owner, flags);
        out.writeTypedList(tags);
        out.writeTypedList(evaluations);
        out.writeTypedList(medias);
        out.writeTypedList(participants);
        out.writeString(address);
    }

    public HashMap<String, String> toHashMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", this.name);
        hashMap.put("description", this.description);
        hashMap.put("address", this.address);
        hashMap.put("visibility", String.valueOf(this.visibility));
        hashMap.put("capacity", String.valueOf(this.capacity));
        hashMap.put("recurrence", String.valueOf(this.recurrence));
        hashMap.put("start", String.valueOf(this.start));
        hashMap.put("end", String.valueOf(this.end));
        hashMap.put("pictureId", String.valueOf(this.picture.getId()));
        hashMap.put("longitude", String.valueOf(this.longitude));
        hashMap.put("latitude", String.valueOf(this.latitude));

        return hashMap;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int recurrence) {
        this.recurrence = recurrence;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Media getPicture() {
        return picture;
    }

    public void setPicture(Media picture) {
        this.picture = picture;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public ArrayList<User> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<User> participants) {
        this.participants = participants;
    }

    public ArrayList<Media> getMedias() {
        return medias;
    }

    public void setMedias(ArrayList<Media> medias) {
        this.medias = medias;
    }

    public ArrayList<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(ArrayList<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}